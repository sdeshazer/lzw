#include <stdlib.h>
#include "dict.h"


//-------------------------------------------
Dict * newDict(unsigned long long hashSize) {

    int i;

    Dict * node;
    for (i = 0; i <= 256; i++) { // ASCII
        node = (Dict*)malloc(Dict);
        node->hashSize = hashSize;

    }//for

    return node;

}//newDict


//-------------------------------
void deleteDictDeep(Dict* dict) {

    int i;

    for(i = 0; i <= (dict->hashSize); i++){

        free(dict->table[i]);

    }//for
}//deleteDictDeep


//-------------------------
bool searchDict(Dict* dict,
                Sequence* key,
                unsigned int* code) {

    //see if table contains the key:
    if (dict->table[&code] == key){

      return true;

    }else{

      return false;
    }//else
}//searchDict


//-----------------------
void insertDict(Dict* dict,
                Sequence* key,
                unsigned int code) {


    //insert key with associated code:
    //we assume that key does not already exist:
    dict->table[code] = key;


}//insertDict
